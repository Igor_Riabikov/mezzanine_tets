FROM python:3.8

RUN apt-get update
RUN apt-get install git


RUN mkdir /cms
WORKDIR /cms

COPY ./ ./

RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

CMD python ./mezzanine_test/manage.py runserver 0:8080